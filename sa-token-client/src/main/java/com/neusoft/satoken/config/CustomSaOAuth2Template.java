package com.neusoft.satoken.config;

import cn.dev33.satoken.oauth2.logic.SaOAuth2Template;
import cn.dev33.satoken.oauth2.model.SaClientModel;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomSaOAuth2Template  extends SaOAuth2Template {


    @Autowired
    SaOauth2AppInfo saOauth2AppInfo;
    // ------------------- 获取数据 (开发者必须重写的函数)
    /**
     * 根据id获取Client信息
     * @param clientId 应用id
     * @return ClientModel
     */
    @Override
    public SaClientModel getClientModel(String clientId) {
        SaClientModel saClientModel = new SaClientModel();
        saClientModel.setClientId(clientId);
        return saClientModel;
    }
    /**
     * 根据ClientId 和 LoginId 获取openid
     * @param clientId 应用id
     * @param loginId 账号id
     * @return 此账号在此Client下的openid
     */
    @Override
    public String getOpenid(String clientId, Object loginId) {
        return null;
    }

    @Override
    public String randomAccessToken(String clientId, Object loginId, String scope) {
        String tokenValue = StpUtil.createLoginSession(loginId);
        return tokenValue;
    }
}
